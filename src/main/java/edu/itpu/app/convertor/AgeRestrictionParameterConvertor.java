package edu.itpu.app.convertor;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.exceptions.ParameterConvertionException;
import edu.itpu.app.parameter.AgeRestrictionParameter;
import edu.itpu.app.parameter.Parameter;

public class AgeRestrictionParameterConvertor<T extends Toy> implements ParameterConvertor<T> {

    @Override
    public Parameter<T> convert(String request) throws ParameterConvertionException {
        if (request == null || request.isBlank())
            throw new ParameterConvertionException("Age restriction parameter required");
        try {
            AgeRestrictionType ageRestrictionType = AgeRestrictionType.valueOf(request);
            return new AgeRestrictionParameter<>(ageRestrictionType);
        } catch (IllegalArgumentException e) {
            throw new ParameterConvertionException("Invalid age restriction type entered");
        }
    }

}
