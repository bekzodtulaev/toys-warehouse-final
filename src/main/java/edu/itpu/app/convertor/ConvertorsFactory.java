package edu.itpu.app.convertor;

import edu.itpu.app.entity.Toy;

public enum ConvertorsFactory {
    PRICE(new PriceParameterConvertor<>()),
    AGE_RESTRICTION(new AgeRestrictionParameterConvertor<>()),
    NAME(new NameParameterConvertor<>());


    private final ParameterConvertor<Toy> convertor;

    ConvertorsFactory(ParameterConvertor<Toy> convertor) {
        this.convertor = convertor;
    }

    public ParameterConvertor<Toy> getConvertor() {
        return convertor;
    }
}
