package edu.itpu.app.convertor;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.exceptions.ParameterConvertionException;
import edu.itpu.app.parameter.Parameter;

public interface ParameterConvertor<T extends Toy> {
    Parameter<T> convert(String request) throws ParameterConvertionException;
}
