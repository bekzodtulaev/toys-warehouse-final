package edu.itpu.app.convertor;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.exceptions.ParameterConvertionException;
import edu.itpu.app.parameter.Parameter;
import edu.itpu.app.parameter.PriceParameter;
import edu.itpu.app.parameter.Range;

public class PriceParameterConvertor<T extends Toy> implements ParameterConvertor<T> {
    @Override
    public Parameter<T> convert(String request) throws ParameterConvertionException {
        if (request == null || request.isBlank()) {
            throw new ParameterConvertionException("price parameter required");
        }

        try {
            String[] parts = request.split("-");
            if (parts.length != 2)
                throw new ParameterConvertionException("Invalid price range");
            double from = Double.parseDouble(parts[0]);
            double to = Double.parseDouble(parts[1]);
            return new PriceParameter<>(new Range<>(from, to));
        } catch (NumberFormatException e) {
            throw new ParameterConvertionException("Invalid price range format");
        }
    }

}
