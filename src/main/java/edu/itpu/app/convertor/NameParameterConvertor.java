package edu.itpu.app.convertor;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.exceptions.ParameterConvertionException;
import edu.itpu.app.parameter.NameParameter;
import edu.itpu.app.parameter.Parameter;

public class NameParameterConvertor<T extends Toy> implements ParameterConvertor {
    @Override
    public Parameter convert(String request) throws ParameterConvertionException {
        if (request == null || request.isBlank())
            throw new ParameterConvertionException("Name field is required");
        return new NameParameter<>(request.toLowerCase());
    }

}
