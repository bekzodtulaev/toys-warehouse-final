package edu.itpu.app.dto;

import edu.itpu.app.entity.auth.Role;
import edu.itpu.app.entity.auth.User;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SessionUser {
    private static SessionUser instance;

    private User sessionUser;
    private Role role;

    @Override
    public String toString() {
        return "SessionUser{" +
                "sessionUser=" + sessionUser +
                ", role=" + role +
                '}';
    }

    public static SessionUser getInstance() {
        if (instance == null) {
            instance = new SessionUser();
        }
        return instance;
    }
}
