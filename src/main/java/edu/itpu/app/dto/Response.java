package edu.itpu.app.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;


@Getter
@Setter
public class Response<T> {
    private ResponseStatus status;

    private String message;

    private Collection<T> data;

    public Response(ResponseStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public Response(ResponseStatus status, Collection<T> data) {
        this.status = status;
        this.data = data;
    }
}
