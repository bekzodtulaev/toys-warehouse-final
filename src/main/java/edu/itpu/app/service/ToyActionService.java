package edu.itpu.app.service;

import java.util.Map;

public interface ToyActionService {
    String create(Map<String, String> params);

    Boolean delete(Map<String, String> params);
}
