package edu.itpu.app.service;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.repository.criteria.SearchCriteria;

import java.util.Collection;

public interface ToyService {
    <A extends Toy> Collection<A> find(SearchCriteria<A> criteria);

    <A extends Toy> Collection<A> findAll(String sort);
}
