package edu.itpu.app.service;

import edu.itpu.app.dto.SessionUser;
import edu.itpu.app.entity.ElectronicToy;
import edu.itpu.app.entity.SoftToy;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.entity.auth.Role;
import edu.itpu.app.entity.auth.User;
import edu.itpu.app.entity.auth.UserRole;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.exceptions.InvalidParameterException;
import edu.itpu.app.repository.AuthDao;
import edu.itpu.app.repository.DaoFactory;
import edu.itpu.app.repository.ToyActionDao;
import edu.itpu.app.repository.criteria.SearchCriteria;

import java.util.*;

public enum ServiceFactory {
    INSTANCE;

    private final DaoFactory daoFactory = DaoFactory.INSTANCE;

    public ToyService getToyService() {
        return new ToyService() {
            @Override
            public <A extends Toy> Collection<A> find(SearchCriteria<A> criteria) {
                return daoFactory.getToyDao(criteria.getProductType()).find(criteria);
            }

            @Override
            public <A extends Toy> Collection<A> findAll(String sorter) {
                List<Toy> toys = new ArrayList<>();
                String[] sort = sorter.split("-");
                toys.addAll(daoFactory.getToyDao(SoftToy.class).findAll(sort[0]));
                toys.addAll(daoFactory.getToyDao(ElectronicToy.class).findAll(sort[0]));

                Comparator<Toy> comparator = null;
                String sortingOrder = sort[0];
                if (sortingOrder.equals("name"))
                    comparator = Comparator.comparing(Toy::getName);
                else if (sortingOrder.equals("price")) {
                    comparator = Comparator.comparing(Toy::getPrice);
                } else {
                    throw new InvalidParameterException("Sorting order is required");
                }


                if (sort[1].equals("desc"))
                    comparator = comparator.reversed();

                toys.sort(comparator);
                return (Collection<A>) toys;
            }
        };
    }

    public AuthService getAuthService() {
        return new AuthService() {
            @Override
            public void login(String username, String password) {
                AuthDao dao = daoFactory.getAuthDao();
                List<User> users = dao.getUsers();
                User userByUsername = users.stream().filter(user
                        -> user.getUsername().equals(username)).findFirst().orElseThrow(() -> new RuntimeException("User not found!"));
                if (!userByUsername.getPassword().equals(password)) {
                    throw new InvalidParameterException("Wrong password entered!");
                }

                SessionUser instance = SessionUser.getInstance();
                instance.setSessionUser(userByUsername);
                instance.setRole(getRoleByUser(userByUsername.getId()));
            }

            @Override
            public Role getRoleByUser(Long userId) {
                AuthDao dao = daoFactory.getAuthDao();

                List<UserRole> userRoles = dao.getAllUserRole();
                UserRole userRoleByUserId = userRoles.stream().filter(userRole
                        -> userRole.getUserId().equals(userId)).findFirst().orElseThrow(() -> new InvalidParameterException("User-role not found!"));
                return dao.getRoles().stream().filter(role
                        -> role.getId().equals(userRoleByUserId.getRoleId())).findFirst().orElseThrow(() -> new InvalidParameterException("Role not found!"));
            }

            @Override
            public List<User> getUsers() {
                return daoFactory.getAuthDao().getUsers();
            }

            @Override
            public Collection<Role> getRoles() {
                return daoFactory.getAuthDao().getRoles();
            }
        };
    }

    public ToyActionService getToyActionService() {
        return new ToyActionService() {
            @Override
            public String create(Map<String, String> params) {
                String name = params.get("name");
                if (name.isBlank())
                    throw new InvalidParameterException("Name can not be null!");

                String price = params.get("price");
                if (price.isBlank())
                    throw new InvalidParameterException("Price can not be null!");

                String quantity = params.get("quantity");
                if (quantity.isBlank())
                    throw new InvalidParameterException("Quantity can not be null!");

                String ageRestrictionType = params.get("ageRestrictionType");
                if (ageRestrictionType.isBlank())
                    throw new InvalidParameterException("AgeRestrictionType can not be null!");

                String manufacturer = params.get("manufacturer");
                if (manufacturer.isBlank())
                    throw new InvalidParameterException("Manufacturer can not be null!");

                String operation = params.get("operation");
                ToyActionDao dao = daoFactory.getToyActionDao();
                if (operation.equals("1")) {
                    SoftToy toy = new SoftToy();
                    toy.setName(name);
                    String material = params.get("material");
                    if (material.isBlank())
                        throw new InvalidParameterException("Material can not be null!");

                    toy.setPrice(Double.parseDouble(price));
                    toy.setQuantity(Integer.valueOf(quantity));
                    if (ageRestrictionType.equals("1"))
                        toy.setAgeRestrictionType(AgeRestrictionType.PG_3);
                    else if (ageRestrictionType.equals("2"))
                        toy.setAgeRestrictionType(AgeRestrictionType.PG_7);
                    else
                        toy.setAgeRestrictionType(AgeRestrictionType.PG_11);
                    toy.setManufacturer(manufacturer);
                    toy.setMaterial(material);
                    return dao.create(operation, toy);
                } else {
                    ElectronicToy toy = new ElectronicToy();
                    toy.setName(name);
                    String batterySize = params.get("batterySize");

                    toy.setQuantity(Integer.valueOf(quantity));
                    toy.setPrice(Double.parseDouble(price));
                    if (ageRestrictionType.equals("1"))
                        toy.setAgeRestrictionType(AgeRestrictionType.PG_3);
                    else if (ageRestrictionType.equals("2"))
                        toy.setAgeRestrictionType(AgeRestrictionType.PG_7);
                    else
                        toy.setAgeRestrictionType(AgeRestrictionType.PG_11);
                    toy.setManufacturer(manufacturer);
                    toy.setBatterySize(batterySize);
                    return dao.create(operation, toy);
                }
            }

            @Override
            public Boolean delete(Map<String, String> params) {
                String id = params.get("id");
                if (id.isBlank())
                    throw new InvalidParameterException("Id can not be null!");
                String operation = params.get("operation");
                ToyActionDao dao = daoFactory.getToyActionDao();
                if (operation.equals("1"))
                    dao.delete("soft-toy", id);
                else
                    dao.delete("electronic-toy", id);
                return true;
            }
        };
    }
}
