package edu.itpu.app.service;


import javax.management.relation.Role;
import java.util.Collection;
import java.util.List;

public interface AuthService {
    void login(String username, String password);

    Role getRoleByUser(Long userId);

    List<User> getUsers();

    Collection<Role> getRoles();
}
