package edu.itpu.app.controller;

import edu.itpu.app.convertor.ConvertorsFactory;
import edu.itpu.app.convertor.ParameterConvertor;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.service.ServiceFactory;

import java.util.Map;

public enum ControllerFactory {
    INSTANCE;

    public SoftToyController<Toy> getSoftToyController() {
        return new SoftToyController<>(ServiceFactory.INSTANCE.getToyService(), getConvertors());
    }

    public ElectronicToyController<Toy> getElectronicToyController() {
        return new ElectronicToyController<>(ServiceFactory.INSTANCE.getToyService(), getConvertors());
    }

    public ToyController<Toy> getToyController() {
        return new ToyController<>(ServiceFactory.INSTANCE.getToyService(), getConvertors());
    }

    public AuthController getAuthController() {
        return new AuthController(ServiceFactory.INSTANCE.getAuthService());
    }

    public ToyActionController getToyActionController() {
        return new ToyActionController(ServiceFactory.INSTANCE.getToyActionService());
    }

    private Map<String, ParameterConvertor<Toy>> getConvertors() {
        return Map.of(
                "name", ConvertorsFactory.NAME.getConvertor(),
                "price", ConvertorsFactory.PRICE.getConvertor(),
                "ageRestriction", ConvertorsFactory.AGE_RESTRICTION.getConvertor()
        );
    }
}
