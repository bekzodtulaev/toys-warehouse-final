package edu.itpu.app.controller;

import edu.itpu.app.convertor.ParameterConvertor;
import edu.itpu.app.dto.Response;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.ResponseStatus;
import edu.itpu.app.service.ToyService;

import java.util.Collection;
import java.util.Map;

public class ToyController<T extends Toy> extends ConcreteController<T> {
    protected ToyController(ToyService toyService, Map<String, ParameterConvertor<T>> parameterConverters) {
        super(toyService, parameterConverters);
    }

    @Override
    public Response<T> find(Map<String, String> requestParams) {
        throw new UnsupportedOperationException("operation is not supported");
    }

    @Override
    public Response<T> findAll(String sortOrder) {
        if (sortOrder.isBlank()) {
            return new Response<>(ResponseStatus.FAILURE, "Please enter valid sorting order");
        }
        Collection<Toy> toys = toyService.findAll(sortOrder);
        if (toys.isEmpty()) {
            return new Response<T>(ResponseStatus.FAILURE, "Warehouse is empty");
        }
        return (Response<T>) new Response<>(ResponseStatus.SUCCESS, toys);
    }
}
