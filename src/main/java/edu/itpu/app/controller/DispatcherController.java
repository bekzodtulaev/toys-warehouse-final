package edu.itpu.app.controller;

import edu.itpu.app.common.RequestParser;
import edu.itpu.app.dto.Response;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.ResponseStatus;
import edu.itpu.client.helper.ResponseListener;

import java.util.HashMap;
import java.util.Map;

public class DispatcherController {
    private static DispatcherController controller;

    private final Map<String, ConcreteController<Toy>> controllerMap = new HashMap<>();

    private DispatcherController() {
        controllerMap.put("/search/soft-toys", ControllerFactory.INSTANCE.getSoftToyController());
        controllerMap.put("/search/electronic-toys", ControllerFactory.INSTANCE.getElectronicToyController());
        controllerMap.put("/toys", ControllerFactory.INSTANCE.getToyController());
    }

    public static DispatcherController getInstance() {
        if (controller == null) {
            controller = new DispatcherController();
        }
        return controller;
    }

    public void listen(String request, ResponseListener responseListener) {
        ConcreteController<Toy> toyConcreteController = controllerMap.get(RequestParser.getUrl(request));
        try {
            if (request.startsWith("/toy")) {
                Response<Toy> response = toyConcreteController.findAll(RequestParser.getSorts(request));
                responseListener.onResponse(response);
            } else {
                Response<Toy> response = toyConcreteController.find(RequestParser.getParams(request));
                responseListener.onResponse(response);
            }
        } catch (Exception e) {
            responseListener.onResponse(new Response(ResponseStatus.FAILURE, e.getMessage()));
        }
    }

    public void listenAuth(String path, Map<String, String> params) {
        try {
            AuthController authController = ControllerFactory.INSTANCE.getAuthController();
            String methodPath = getMethodPath(path);
            if (methodPath.startsWith("login")) {
                ResponseListener.printResponse(authController.login(params), null);
            } else if (methodPath.startsWith("users")) {
                ResponseListener.printResponse(authController.getUsers(), "users");
            } else if (methodPath.startsWith("roles")) {
                ResponseListener.printResponse(authController.roles(), "roles");
            } else {
                throw new RuntimeException("Method not supported!");
            }
        } catch (Exception e) {
            ResponseListener.printResponse(new Response(ResponseStatus.FAILURE, e.getMessage()), path);
        }
    }

    private static String getMethodPath(String path) {
        String[] split = path.split("/");
        if (split.length > 2)
            return split[2];
        return "";
    }

    public void listenToyAction(String path, Map<String, String> params) {
        ToyActionController toyActionController = ControllerFactory.INSTANCE.getToyActionController();
        try {
            if (path.equals("create"))
                ResponseListener.printResponse(toyActionController.create(params), null);
            else if (path.equals("delete"))
                ResponseListener.printResponse(toyActionController.delete(params), null);
            else
                throw new RuntimeException("Method not supported!");
        } catch (Exception e) {
            ResponseListener.printResponse(new Response(ResponseStatus.FAILURE, e.getMessage()), path);
        }
    }
}
