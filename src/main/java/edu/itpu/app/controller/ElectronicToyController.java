package edu.itpu.app.controller;

import edu.itpu.app.convertor.ParameterConvertor;
import edu.itpu.app.dto.Response;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.ResponseStatus;
import edu.itpu.app.exceptions.ParameterConvertionException;
import edu.itpu.app.parameter.Parameter;
import edu.itpu.app.repository.criteria.ElectronicToySearchCriteria;
import edu.itpu.app.repository.criteria.SearchCriteria;
import edu.itpu.app.service.ToyService;

import java.util.Collection;
import java.util.List;
import java.util.Map;

public class ElectronicToyController<T extends Toy> extends ConcreteController<T> {
    protected ElectronicToyController(ToyService toyService, Map<String, ParameterConvertor<T>> parameterConverters) {
        super(toyService, parameterConverters);
    }

    @Override
    public Response<T> find(Map<String, String> requestParams) {
        SearchCriteria<T> criteria = new ElectronicToySearchCriteria<>();

        for (Map.Entry<String, String> entry : requestParams.entrySet()) {
            ParameterConvertor<T> converter = parameterConverters.get(entry.getKey());
            if (converter != null) {
                try {
                    Parameter<T> parameter = converter.convert(entry.getValue());
                    criteria.add(parameter);
                } catch (ParameterConvertionException e) {
                    return new Response<>(ResponseStatus.FAILURE, e.getMessage());
                }
            }
        }

        Collection<T> toys = toyService.find(criteria);
        if (toys.isEmpty())
            return new Response<>(ResponseStatus.FAILURE, "Product not found with given parameters");

        return new Response<>(ResponseStatus.SUCCESS, toys);
    }

    @Override
    public Response<T> findAll(String sortOrder) {
        throw new UnsupportedOperationException("find all is not supported in Electronic toys controller");
    }

}
