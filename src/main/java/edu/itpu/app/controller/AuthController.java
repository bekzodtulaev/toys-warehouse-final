package edu.itpu.app.controller;

import edu.itpu.app.dto.Response;
import edu.itpu.app.entity.auth.Role;
import edu.itpu.app.entity.auth.User;
import edu.itpu.app.enums.ResponseStatus;
import edu.itpu.app.exceptions.InvalidParameterException;
import edu.itpu.app.service.AuthService;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class AuthController {

    private AuthService service;

    public Response<String> login(Map<String, String> params) {
        String username = params.get("username");
        if (username.isBlank())
            throw new InvalidParameterException("Username can not be null!");

        String password = params.get("password");
        if (password.isBlank())
            throw new InvalidParameterException("Password can not be null!");
        service.login(username, password);
        return new Response<>(ResponseStatus.SUCCESS, "You are successfully logged in!");
    }

    public Response<User> getUsers() {
        return new Response<>(ResponseStatus.SUCCESS, service.getUsers());
    }

    public Response<Role> roles() {
        return new Response<>(ResponseStatus.SUCCESS, service.getRoles());
    }
}
