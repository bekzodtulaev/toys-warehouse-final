package edu.itpu.app.controller;

import edu.itpu.app.dto.Response;
import edu.itpu.app.enums.ResponseStatus;
import edu.itpu.app.service.ToyActionService;
import lombok.AllArgsConstructor;

import java.util.Map;

@AllArgsConstructor
public class ToyActionController {

    private ToyActionService service;

    public Response<String> create(Map<String, String> params) {
        return new Response<>(ResponseStatus.SUCCESS, service.create(params));
    }

    public Response<String> delete(Map<String, String> params) {
        return new Response<>(ResponseStatus.SUCCESS, String.valueOf(service.delete(params)));
    }
}
