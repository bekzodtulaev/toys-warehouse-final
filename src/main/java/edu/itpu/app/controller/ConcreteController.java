package edu.itpu.app.controller;

import edu.itpu.app.convertor.ParameterConvertor;
import edu.itpu.app.dto.Response;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.service.ToyService;

import java.util.Map;

public abstract class ConcreteController<A extends Toy> {
    protected final ToyService toyService;

    protected final Map<String, ParameterConvertor<A>> parameterConverters;

    protected ConcreteController(ToyService toyService, Map<String, ParameterConvertor<A>> parameterConverters) {
        this.toyService = toyService;
        this.parameterConverters = parameterConverters;
    }

    public abstract Response<A> find(Map<String, String> requestParams);

    public abstract Response<A> findAll(String sortOrder);
}
