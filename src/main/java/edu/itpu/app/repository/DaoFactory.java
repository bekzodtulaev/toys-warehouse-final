package edu.itpu.app.repository;

import edu.itpu.app.entity.ElectronicToy;
import edu.itpu.app.entity.SoftToy;
import edu.itpu.app.entity.Toy;

public enum DaoFactory {
    INSTANCE;


    public <A extends Toy> ToyDao<A> getToyDao(Class<A> toyClass) {
        if (SoftToy.class.equals(toyClass)) {
            return (ToyDao<A>) new SoftToyDao("src/main/resources/soft-toys.csv");
        }

        if (ElectronicToy.class.equals(toyClass)) {
            return (ToyDao<A>) new ElectronicToyDao("src/main/resources/electronic-toys.csv");
        }
        return null;
    }

    public AuthDao getAuthDao() {
        return new AuthDao();
    }

    public ToyActionDao getToyActionDao() {
        return new ToyActionDao();
    }
}
