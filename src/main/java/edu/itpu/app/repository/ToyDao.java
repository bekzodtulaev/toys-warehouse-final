package edu.itpu.app.repository;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.repository.criteria.SearchCriteria;

import java.util.Collection;

public interface ToyDao<T extends Toy> {
    Collection<T> find(SearchCriteria<T> searchCriteria);
    Collection<T> findAll(String sortingOrder);

}
