package edu.itpu.app.repository;

import edu.itpu.app.entity.auth.Role;
import edu.itpu.app.entity.auth.User;
import edu.itpu.app.entity.auth.UserRole;
import edu.itpu.client.helper.CustomFileHandler;
import edu.itpu.client.helper.CustomMapper;

import java.io.IOException;
import java.util.List;

public class AuthDao {

    public List<User> getUsers() {
        try {
            return CustomMapper.toUsers(CustomFileHandler.readFile("src/main/resources/users.csv"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Role> getRoles() {
        try {
            return CustomMapper.toRoles(CustomFileHandler.readFile("src/main/resources/roles.csv"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<UserRole> getAllUserRole() {
        try {
            return CustomMapper.toUserRoles(CustomFileHandler.readFile("src/main/resources/user_roles.csv"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
