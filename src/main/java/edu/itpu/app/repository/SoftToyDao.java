package edu.itpu.app.repository;

import edu.itpu.app.entity.SoftToy;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.repository.csvLineParser.CSVLineParser;

import java.util.List;

public class SoftToyDao extends AbstractDao<SoftToy> {

    public SoftToyDao(String path) {
        super(path, new CSVLineParser<>(SoftToy::new, List.of(
                CSVLineParser.forInt(Toy::setId),
                CSVLineParser.forString(Toy::setName),
                CSVLineParser.forDouble(Toy::setPrice),
                CSVLineParser.forString(SoftToy::setMaterial),
                CSVLineParser.forInt(Toy::setQuantity),
                CSVLineParser.forEnum(AgeRestrictionType.class, Toy::setAgeRestrictionType),
                CSVLineParser.forString(Toy::setManufacturer)
        )));
    }

}
