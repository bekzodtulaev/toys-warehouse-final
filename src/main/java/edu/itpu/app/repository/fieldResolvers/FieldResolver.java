package edu.itpu.app.repository.fieldResolvers;

public interface FieldResolver<T> {
    void parse(T target, String value) throws IllegalArgumentException;

}
