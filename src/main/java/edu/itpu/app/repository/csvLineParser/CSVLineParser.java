package edu.itpu.app.repository.csvLineParser;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.repository.fieldResolvers.FieldResolver;

import java.text.ParseException;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class CSVLineParser<T extends Toy> {
    private final Supplier<T> supplier;

    private final List<FieldResolver<T>> fieldResolvers;

    public CSVLineParser(Supplier<T> supplier, List<FieldResolver<T>> fieldResolvers) {
        this.supplier = supplier;
        this.fieldResolvers = fieldResolvers;
    }

    public T parseLine(String[] values) throws ParseException {
        T toy = supplier.get();
        int temp = 0;
        for (FieldResolver<T> fieldResolver : fieldResolvers) {
            try {
                fieldResolver.parse(toy, values[temp++].trim());
            } catch (Exception e) {
                throw new ParseException(e.getMessage(), temp);
            }
        }
        return toy;
    }

    public static <T> FieldResolver<T> forDouble(BiConsumer<T,Double> setter) {
        return (target, value) -> setter.accept(target, Double.parseDouble(value));
    }

    public static <T> FieldResolver<T> forInt(BiConsumer<T,Integer> setter) {
        return (target, value) -> setter.accept(target, Integer.parseInt(value));
    }

    public static <T> FieldResolver<T> forString(BiConsumer<T,String> setter) {
        return setter::accept;
    }

    public static <E extends Enum<E>, T> FieldResolver<T> forEnum(Class<E> enumClass, BiConsumer<T, E> setter) {
        return (target, value) -> {
            E temp = Enum.valueOf(enumClass, value);
            setter.accept(target, temp);
        };
    }

}
