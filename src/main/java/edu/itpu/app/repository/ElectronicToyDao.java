package edu.itpu.app.repository;

import edu.itpu.app.entity.ElectronicToy;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.repository.csvLineParser.CSVLineParser;

import java.util.List;

public class ElectronicToyDao extends AbstractDao<ElectronicToy> {
    public ElectronicToyDao(String path) {
        super(path, new CSVLineParser<>(ElectronicToy::new, List.of(
                CSVLineParser.forInt(Toy::setId),
                CSVLineParser.forString(Toy::setName),
                CSVLineParser.forDouble(Toy::setPrice),
                CSVLineParser.forInt(Toy::setQuantity),
                CSVLineParser.forString(ElectronicToy::setBatterySize),
                CSVLineParser.forEnum(AgeRestrictionType.class, Toy::setAgeRestrictionType),
                CSVLineParser.forString(Toy::setManufacturer)
        )));
    }

}
