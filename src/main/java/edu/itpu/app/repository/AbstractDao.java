package edu.itpu.app.repository;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.repository.criteria.SearchCriteria;
import edu.itpu.app.repository.csvLineParser.CSVLineParser;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractDao<T extends Toy> implements ToyDao<T> {
    private final String csvPath;

    private final CSVLineParser<T> parser;


    protected AbstractDao(String path, CSVLineParser<T> parser) {
        this.csvPath = path;
        this.parser = parser;
    }


    @Override
    public Collection<T> find(SearchCriteria<T> searchCriteria) {
        try {
            CSVReader reader = new CSVReader(new FileReader(csvPath));
            List<String[]> lines = reader.readAll();
            List<T> toys = new ArrayList<>();

            for (int i = 1; i < lines.size(); i++) {
                T product;
                try {
                    product = parser.parseLine(lines.get(i));
                } catch (ParseException e) {
                    //Skip toy if it can not be parsed
                    continue;
                }

                if (searchCriteria.test(product))
                    toys.add(product);
            }

            return toys;
        } catch (IOException e) {
            throw new RuntimeException("Failed to read CSV file: " + csvPath);
        } catch (CsvException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public Collection<T> findAll(String sortingOrder) {
        List<T> toys = new ArrayList<>();
        try {
            CSVReader reader = new CSVReader(new FileReader(csvPath));
            List<String[]> lines = reader.readAll();

            for (int i = 1; i < lines.size(); i++) {
                T toy;
                try {
                    toy = parser.parseLine(lines.get(i));
                } catch (ParseException e) {
                    //Skip toy if it can not be parsed
                    continue;
                }

                toys.add(toy);
            }

        } catch (IOException | CsvException e) {
            throw new RuntimeException("Failed to read CSV file: " + csvPath);
        }
        return toys;
    }

}
