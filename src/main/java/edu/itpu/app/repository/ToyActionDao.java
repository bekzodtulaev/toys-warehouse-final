package edu.itpu.app.repository;

import edu.itpu.app.entity.Toy;
import edu.itpu.client.helper.CustomFileHandler;

import java.io.IOException;

public class ToyActionDao {

    public String create(String operation, Toy toy) {
        try {
            String path;
            if (operation.equals("1")) {
                path = "src/main/resources/soft-toys.csv";
            } else {
                path = "src/main/resources/electronic-toys.csv";
            }
            toy.setId(CustomFileHandler.generateId(path));
            CustomFileHandler.writeToFile(path, toy.toLine());
            return toy.getId().toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void delete(String operation, String id) {
        try {
            if (operation.equals("soft-toy"))
                CustomFileHandler.delete("src/main/resources/soft-toys.csv", Long.valueOf(id),
                        "ID,NAME,PRICE,QUANTITY,AGE_RESTRICTION_TYPE,MANUFACTURER,MATERIAL");
            else
                CustomFileHandler.delete("src/main/resources/electronic-toys.csv", Long.valueOf(id),
                        "ID,NAME,PRICE,QUANTITY,BATTERY_SIZE,RESTRICTION_TYPE,MANUFACTURER");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
