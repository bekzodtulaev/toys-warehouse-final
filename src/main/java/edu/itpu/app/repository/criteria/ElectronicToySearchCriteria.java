package edu.itpu.app.repository.criteria;

import edu.itpu.app.entity.ElectronicToy;
import edu.itpu.app.entity.Toy;

public class ElectronicToySearchCriteria<T extends Toy> extends AbstractCriteria<T> {
    @Override
    public Class<T> getProductType() {
        return (Class<T>) ElectronicToy.class;
    }

}
