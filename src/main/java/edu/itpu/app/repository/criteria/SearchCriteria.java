package edu.itpu.app.repository.criteria;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.parameter.Parameter;

public interface SearchCriteria<T extends Toy> {
    Class<T> getProductType();
    <A extends Parameter<T>> SearchCriteria<T> add(A parameter);
    boolean test(T product);

}
