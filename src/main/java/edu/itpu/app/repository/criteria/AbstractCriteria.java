package edu.itpu.app.repository.criteria;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.parameter.Parameter;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractCriteria<T extends Toy> implements SearchCriteria<T> {
    protected final Map<Class<?>, Parameter<T>> parameters = new HashMap<>();


    @Override
    public <A extends Parameter<T>> SearchCriteria<T> add(A parameter) {
        parameters.put(parameter.getClass(), parameter);
        return this;
    }

    @Override
    public boolean test(T product) {
        return parameters.values()
                .stream()
                .allMatch(p -> p.test(product));
    }

}
