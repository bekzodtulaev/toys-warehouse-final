package edu.itpu.app.repository.criteria;

import edu.itpu.app.entity.SoftToy;
import edu.itpu.app.entity.Toy;

public class SoftToySearchCriteria<T extends Toy> extends AbstractCriteria<T> {
    @Override
    public Class<T> getProductType() {
        return (Class<T>) SoftToy.class;
    }

}
