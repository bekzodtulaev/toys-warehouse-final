package edu.itpu.app.exceptions;

public class InvalidParameterException extends RuntimeException {
    public InvalidParameterException(String info) {
        super(info);
    }

}
