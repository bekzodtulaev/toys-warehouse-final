package edu.itpu.app.exceptions;

public class ParameterConvertionException extends RuntimeException {
    public ParameterConvertionException(String info) {
        super(info);
    }

}
