package edu.itpu.app.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class SoftToy extends Toy {
    private String material;

    @Override
    public String toString() {
        return super.toString() +
                ", material='" + material;
    }

    public String toLine() {
        return id + "," + name + "," + price + "," + material + "," + quantity + "," + ageRestrictionType + "," + manufacturer;
    }
}
