package edu.itpu.app.entity;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
public class ElectronicToy extends Toy {
    private String batterySize;

    @Override
    public String toString() {
        return super.toString() +
                ", batterySize='" + batterySize;
    }

    public String toLine() {
        return id + "," + name + "," + price + "," + quantity + "," + batterySize + "," + ageRestrictionType + "," + manufacturer;
    }
}
