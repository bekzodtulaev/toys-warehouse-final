package edu.itpu.app.entity;

import edu.itpu.app.enums.AgeRestrictionType;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class Toy implements Serializable {
    protected Integer id;

    protected String name;

    protected Double price;

    protected Integer quantity;

    protected AgeRestrictionType ageRestrictionType;

    protected String manufacturer;

    @Override
    public String toString() {
        return "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                ", ageRestrictionType=" + ageRestrictionType +
                ", manufacturer='" + manufacturer;
    }

    public String toLine() {
        return id + "," + name + "," + price + "," + quantity + "," + ageRestrictionType + "," + manufacturer + ",";
    }
}
