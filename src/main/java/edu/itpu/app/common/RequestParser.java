package edu.itpu.app.common;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class RequestParser {
    public static String getUrl(String request) {
        return request.substring(0, request.indexOf("?"));
    }

    public static Map<String, String> getParams(String request) {
        Map<String, String> params = new HashMap<>();
        if (Objects.nonNull(request) && !request.isBlank()) {
            String paramStr = request.substring(getUrl(request).length() + 1);
            String[] paramSet = paramStr.split("&");
            for (String param : paramSet) {
                String[] splitParam = param.split("=");
                params.put(splitParam[0], splitParam[1]);
            }
        }
        return params;
    }

    public static String getSorts(String request) {
        if (Objects.nonNull(request) && !request.isBlank()) {
            return request.substring(getUrl(request).length() + 1);
        }
        return null;
    }
}
