package edu.itpu.app.parameter;

import edu.itpu.app.entity.Toy;

public interface Parameter<T extends Toy> {
    boolean test(T product);
}
