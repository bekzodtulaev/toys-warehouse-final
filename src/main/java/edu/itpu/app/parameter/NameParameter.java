package edu.itpu.app.parameter;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.exceptions.InvalidParameterException;

import java.util.Objects;

public record NameParameter<T extends Toy>(String name) implements Parameter<T> {
    public NameParameter {
        if (Objects.isNull(name) || name.isBlank()) {
            throw new InvalidParameterException("name can not blank");
        }
    }

    @Override
    public boolean test(T product) {
        return product.getName().equalsIgnoreCase(name);
    }

}
