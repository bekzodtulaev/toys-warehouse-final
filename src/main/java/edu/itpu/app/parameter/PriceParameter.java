package edu.itpu.app.parameter;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.exceptions.InvalidParameterException;

public record PriceParameter<T extends Toy>(Range<Double> range) implements Parameter<T> {
    public PriceParameter {
        if (range.from() < 0) {
            throw new InvalidParameterException("Price can not be less than 0");
        }
    }

    @Override
    public boolean test(T product) {
        return range.validRange(product.getPrice());
    }

}
