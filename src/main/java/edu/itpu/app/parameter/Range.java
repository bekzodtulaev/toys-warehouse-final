package edu.itpu.app.parameter;


public record Range<T extends Comparable<T>>(T from, T to) {
    public Range {
        if (from == null || to == null || from.compareTo(to) > 0) {
            throw new IllegalArgumentException(from + " must be less than " + to);
        }
    }

    public boolean validRange(T value) {
        return from.compareTo(value) <= 0 && to.compareTo(value) >= 0;
    }

}

