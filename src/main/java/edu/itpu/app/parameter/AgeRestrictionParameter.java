package edu.itpu.app.parameter;

import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.exceptions.InvalidParameterException;

import java.util.Objects;

public record AgeRestrictionParameter<T extends Toy>(AgeRestrictionType ageRestrictionType) implements Parameter<T> {
    public AgeRestrictionParameter {
        if (Objects.isNull(ageRestrictionType)) {
            throw new InvalidParameterException("restriction type can not be null");
        }
    }

    @Override
    public boolean test(T product) {
        return product.getAgeRestrictionType().equals(ageRestrictionType);
    }

}
