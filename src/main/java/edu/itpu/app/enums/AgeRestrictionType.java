package edu.itpu.app.enums;

public enum AgeRestrictionType {
    PG_3,
    PG_7,
    PG_11
}
