package edu.itpu.client;

import edu.itpu.app.dto.SessionUser;
import edu.itpu.client.helper.ConsoleHelper;
import edu.itpu.client.helper.RequestHelper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class UI {

    public static void run() {
        ConsoleHelper.println("\n\n");
        ConsoleHelper.println("1 -->> Login");
        ConsoleHelper.println("0 -->> Exit");

        String operation = ConsoleHelper.readText("Enter operation number: ");

        switch (operation) {
            case "0" -> System.exit(0);
            case "1" -> login();
            default -> ConsoleHelper.printError("Invalid operation number: choose available options");
        }
        run();
    }

    private static void login() {
        Map<String, String> params = new HashMap<>();

        ConsoleHelper.println("\n\n");
        ConsoleHelper.println("1.Username: ");
        params.put("username", ConsoleHelper.readText());

        ConsoleHelper.println("2.Password: ");
        params.put("password", ConsoleHelper.readText());

        RequestHelper.makeAuthRequest("/auth/login", params);
        menu();
    }

    public static void menu() {
        SessionUser instance = SessionUser.getInstance();
        if (!Objects.isNull(instance.getSessionUser())) {
            ConsoleHelper.println("\n\n");
            ConsoleHelper.println("Menu: ");
            ConsoleHelper.println("1.See available products");
            ConsoleHelper.println("2.Search product");
            if (instance.getRole().getCode().equals("ADMIN")) {
                ConsoleHelper.println("3.Create product");
                ConsoleHelper.println("4.Delete product");
                ConsoleHelper.println("5.Show all users");
                ConsoleHelper.println("6.Show all roles");
            }
            ConsoleHelper.println("7.Profile");
            ConsoleHelper.println("0.Logout\n");
            String operation = ConsoleHelper.readText("Enter operation number: ");

            switch (operation) {
                case "0" -> {
                    instance.setSessionUser(null);
                    instance.setRole(null);
                    run();
                }
                case "1" -> showAvailableProduct();
                case "2" -> searchProduct();
                case "3" -> createProduct();
                case "4" -> deleteProduct();
                case "5" -> showAllUser();
                case "6" -> showAllRoles();
                case "7" -> ConsoleHelper.println(SessionUser.getInstance());
                default -> ConsoleHelper.printError("Invalid operation number: choose available options");
            }
            menu();
        } else {
            login();
        }
    }

    private static void showAllRoles() {
        RequestHelper.makeAuthRequest("/auth/roles", null);
    }

    private static void showAllUser() {
        RequestHelper.makeAuthRequest("/auth/users", null);
    }

    private static void deleteProduct() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.println("1.Soft toy");
        ConsoleHelper.println("2.Electronic toy");
        params.put("operation", ConsoleHelper.readText("Enter operation number: "));
        params.put("id", ConsoleHelper.readText("id: "));
        RequestHelper.makeToyActionRequest("delete", params);
    }

    private static void createProduct() {
        Map<String, String> params = new HashMap<>();
        ConsoleHelper.println("1.Soft toy");
        ConsoleHelper.println("2.Electronic toy");
        String operation = ConsoleHelper.readText("Enter operation number: ");
        params.put("operation", operation);

        params.put("name", ConsoleHelper.readText("Name: "));
        params.put("price", ConsoleHelper.readText("Price: "));
        params.put("quantity", ConsoleHelper.readText("Quantity: "));
        params.put("ageRestrictionType", ConsoleHelper.readText("AgeRestrictionType(\n\t1-> PG_3,\n\t2-> PG_7\n\t3-> PG_11\n): "));
        params.put("manufacturer", ConsoleHelper.readText("Manufacturer: "));

        switch (operation) {
            case "1" -> params.put("material", ConsoleHelper.readText("Material: "));
            case "2" -> params.put("batterySize", ConsoleHelper.readText("BatterySize: "));
            default -> {
                ConsoleHelper.printError("Invalid operation number: choose available options");
                return;
            }
        }

        RequestHelper.makeToyActionRequest("create", params);
    }

    public static void showAvailableProduct() {
        StringBuilder requestUrl = new StringBuilder("/toys");
        ConsoleHelper.println("Choose sorting filter: ");
        ConsoleHelper.println("1.Name");
        ConsoleHelper.println("2.Price");
        String operation = ConsoleHelper.readText("enter operation number: ");
        switch (operation) {
            case "1" -> requestUrl.append("?name-");
            case "2" -> requestUrl.append("?price-");
            default -> {
                ConsoleHelper.printError("Invalid operation number");
                return;
            }
        }
        ConsoleHelper.println("Choose sorting order: ");
        ConsoleHelper.println("1.Asc");
        ConsoleHelper.println("2.Desc");
        String option = ConsoleHelper.readText("enter operation number: ");
        switch (option) {
            case "1" -> requestUrl.append("asc");
            case "2" -> requestUrl.append("desc");
            default -> {
                ConsoleHelper.printError("Invalid operation number");
                return;
            }
        }
        RequestHelper.makeRequest(requestUrl.toString());
    }

    public static void searchProduct() {
        Map<String, String> params = new HashMap<>();
        StringBuilder requestUrl = new StringBuilder("/search");
        ConsoleHelper.println("Choose category: ");
        ConsoleHelper.println("1.Soft toys");
        ConsoleHelper.println("2.Electronic toys");

        String operation = ConsoleHelper.readText("Enter operation number: ");
        switch (operation) {
            case "1" -> requestUrl.append("/soft-toys");
            case "2" -> requestUrl.append("/electronic-toys");
            default -> {
                ConsoleHelper.printError("Invalid operation number");
                return;
            }
        }
        String name = ConsoleHelper.readText("Enter name of toys: ");
        if (name.isBlank()) {
            ConsoleHelper.printError("Name is required!");
            return;
        }
        params.put("name", name);

        String price = ConsoleHelper.readText("Enter price range [10-1000 or 6.98-18.99]: ");
        if (price.isBlank()) {
            ConsoleHelper.printError("Price is required");
            return;
        }
        params.put("price", price);

        ConsoleHelper.println("Choose age restriction type:");
        ConsoleHelper.println("1.PG-3:");
        ConsoleHelper.println("2.PG-7");
        ConsoleHelper.println("3.PG-11");
        String value = ConsoleHelper.readText("Enter value: ");
        switch (value) {
            case "1" -> params.put("age", "PG_3");
            case "2" -> params.put("age", "PG_7");
            case "3" -> params.put("age", "PG_11");
            default -> {
                ConsoleHelper.printError("Price is required");
                return;
            }
        }
        RequestHelper.makeRequest(requestUrl.toString(), params);
    }
}
