package edu.itpu.client;

import edu.itpu.client.constants.AppConstants;
import edu.itpu.client.helper.ConsoleHelper;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        ConsoleHelper.println(AppConstants.APP_NAME + " version: " + AppConstants.VERSION + "  " + new Date());
        ConsoleHelper.println("Developer name: " + AppConstants.DEV_NAME);
        ConsoleHelper.println("Developer email: " + AppConstants.DEV_EMAIL);
        UI.run();
    }

}
