package edu.itpu.client.helper;

import edu.itpu.app.controller.DispatcherController;

import java.util.Map;
import java.util.Objects;

public class RequestHelper {
    private static final DispatcherController dispatcherController = DispatcherController.getInstance();

    public static void makeRequest(String url, Map<String, String> params) {
        StringBuilder request = new StringBuilder(url + "?");
        if (Objects.isNull(params) || params.isEmpty()) {
            ConsoleHelper.printError("Bad request");
        }

        for (Map.Entry<String, String> entry : params.entrySet()) {
            request.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        request.deleteCharAt(request.length() - 1);

        dispatcherController.listen(request.toString(), new ResponseListener<>(url));
    }

    public static void makeRequest(String url) {
        dispatcherController.listen(url, new ResponseListener<>(url));
    }

    public static void makeAuthRequest(String path, Map<String, String> params) {
        dispatcherController.listenAuth(path, params);
    }

    public static void makeToyActionRequest(String path, Map<String, String> params) {
        dispatcherController.listenToyAction(path, params);
    }
}
