package edu.itpu.client.helper;

import edu.itpu.app.dto.Response;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.enums.ResponseStatus;

import java.util.Objects;

public class ResponseListener<T extends Toy> {
    private final String requestUrl;

    public ResponseListener(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public void onResponse(Response<T> response) {
        if (requestUrl.startsWith("/toy")) {
            if (response.getStatus() == ResponseStatus.SUCCESS) {
                int temp = 1;
                for (Toy toy : response.getData())
                    ConsoleHelper.println(String.format("%3s | ", temp++) + toy);
            } else {
                ConsoleHelper.printError(response.getMessage());
            }
        } else {
            if (response.getStatus() == ResponseStatus.SUCCESS) {
                int temp = 1;
                for (T toy : response.getData()) {
                    ConsoleHelper.println(String.format("%3s | ", temp++) + toy);
                }
            } else {
                ConsoleHelper.printError(response.getMessage());
            }
        }
    }

    public static void printResponse(Response response, String path) {
        if (!Objects.isNull(path)) {
            if (response.getStatus() == ResponseStatus.SUCCESS) {
                int temp = 1;
                for (Object user : response.getData())
                    ConsoleHelper.println(String.format("%3s | ", temp++) + user);
            } else {
                ConsoleHelper.printError(response.getMessage());
            }
        } else
            ConsoleHelper.println(response.getMessage());
    }
}