package edu.itpu.client.helper;

import edu.itpu.app.entity.auth.Role;
import edu.itpu.app.entity.auth.User;
import edu.itpu.app.entity.auth.UserRole;

import java.util.List;
import java.util.stream.Collectors;

public class CustomMapper {

    public static List<Role> toRoles(List<String> roles) {
        return roles.stream().map(CustomMapper::toRole).collect(Collectors.toList());
    }

    public static Role toRole(String role) {
        String[] strings = role.split(",");
        return new Role(Long.valueOf(strings[0]), strings[1], strings[2]);
    }

    public static List<User> toUsers(List<String> users) {
        return users.stream().map(CustomMapper::toUser).collect(Collectors.toList());
    }

    public static User toUser(String user) {
        String[] strings = user.split(",");
        return new User(Long.valueOf(strings[0]), strings[1], strings[2], strings[3], strings[4], strings[5], strings[6]);
    }

    public static List<UserRole> toUserRoles(List<String> householdGoods) {
        return householdGoods.stream().map(CustomMapper::toUserRole).collect(Collectors.toList());
    }

    public static UserRole toUserRole(String householdGood) {
        String[] strings = householdGood.split(",");
        return new UserRole(Long.valueOf(strings[0]), Long.valueOf(strings[1]));
    }
}
