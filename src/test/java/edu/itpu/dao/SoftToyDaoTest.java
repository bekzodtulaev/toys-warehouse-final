package edu.itpu.dao;

import edu.itpu.app.entity.SoftToy;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.repository.SoftToyDao;
import edu.itpu.app.repository.ToyDao;
import edu.itpu.app.repository.criteria.SearchCriteria;
import edu.itpu.app.repository.criteria.SoftToySearchCriteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

public class SoftToyDaoTest {

    @Test
    void shouldFindAll() {
        SoftToyDao softToyDao = new SoftToyDao("src/test/resources/soft-toys-test.csv");
        Collection<SoftToy> softToys = softToyDao.findAll("Any");
        Collection<SoftToy> expectedResult = List.of(SoftToy.builder()
                        .id(1)
                        .name("Teddy Bear")
                        .price(14.99)
                        .material("Plush")
                        .quantity(500)
                        .ageRestrictionType(AgeRestrictionType.PG_3)
                        .manufacturer("ToyCo")
                        .build(),
                SoftToy.builder()
                        .id(2)
                        .name("Kitten")
                        .price(9.99)
                        .material("Cotton")
                        .quantity(275)
                        .ageRestrictionType(AgeRestrictionType.PG_7)
                        .manufacturer("PetToys")
                        .build(),
                SoftToy.builder()
                        .id(3)
                        .name("Penguin")
                        .price(11.95)
                        .material("Fleece")
                        .quantity(400)
                        .ageRestrictionType(AgeRestrictionType.PG_11)
                        .manufacturer("Softies Inc.")
                        .build());
        Assertions.assertNotNull(softToys);
        Assertions.assertArrayEquals(expectedResult.toArray(), softToys.toArray());
    }

    @Test
    void shouldFindNone(){
        ToyDao<SoftToy> softToyDao = new SoftToyDao("src/test/resources/soft-toys-test.csv");
        SearchCriteria<SoftToy> criteria = new SoftToySearchCriteria<SoftToy>().add(p -> false);
        Collection<SoftToy> softToyCollection = softToyDao.find(criteria);
        Assertions.assertNotNull(softToyCollection);
        Assertions.assertTrue(softToyCollection.isEmpty());
    }

}
