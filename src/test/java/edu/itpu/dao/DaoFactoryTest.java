package edu.itpu.dao;

import edu.itpu.app.entity.SoftToy;
import edu.itpu.app.entity.Toy;
import edu.itpu.app.repository.DaoFactory;
import edu.itpu.app.repository.SoftToyDao;
import edu.itpu.app.repository.ToyDao;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DaoFactoryTest {
    @Test
    void shouldReturnNullWhenUnknownClassIsPassed() {
        class DummyToy extends Toy {
        }
        ToyDao<DummyToy> toyDao = DaoFactory.INSTANCE.getToyDao(DummyToy.class);
        Assertions.assertNull(toyDao);
    }

    @Test
    void shouldReturnSoftToyDao() {
        ToyDao<SoftToy> softToyToyDao = DaoFactory.INSTANCE.getToyDao(SoftToy.class);
        Assertions.assertNotNull(softToyToyDao);
        Assertions.assertTrue(softToyToyDao instanceof SoftToyDao);
    }

}
