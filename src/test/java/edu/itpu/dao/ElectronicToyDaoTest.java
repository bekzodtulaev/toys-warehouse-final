package edu.itpu.dao;

import edu.itpu.app.entity.ElectronicToy;
import edu.itpu.app.enums.AgeRestrictionType;
import edu.itpu.app.repository.ElectronicToyDao;
import edu.itpu.app.repository.ToyDao;
import edu.itpu.app.repository.criteria.ElectronicToySearchCriteria;
import edu.itpu.app.repository.criteria.SearchCriteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

public class ElectronicToyDaoTest {
    @Test
    void shouldFindAll() {
        ElectronicToyDao electronicToyDao = new ElectronicToyDao("src/test/resources/electronic-toys-test.csv");
        Collection<ElectronicToy> electronicToys = electronicToyDao.findAll("Any");
        Collection<ElectronicToy> expectedResult = List.of(ElectronicToy.builder()
                        .id(1)
                        .name("Car")
                        .price(9.99)
                        .quantity(600)
                        .batterySize("AAA")
                        .ageRestrictionType(AgeRestrictionType.PG_7)
                        .manufacturer("ToysFactory")
                        .build(),
                ElectronicToy.builder()
                        .id(2)
                        .name("Robot")
                        .price(24.95)
                        .quantity(200)
                        .batterySize("AA")
                        .ageRestrictionType(AgeRestrictionType.PG_11)
                        .manufacturer("TechToys")
                        .build(),
                ElectronicToy.builder()
                        .id(3)
                        .name("Drone")
                        .price(49.99)
                        .quantity(50)
                        .batterySize("AAA")
                        .ageRestrictionType(AgeRestrictionType.PG_11)
                        .manufacturer("FlyHigh")
                        .build());
        Assertions.assertNotNull(electronicToys);
        Assertions.assertArrayEquals(expectedResult.toArray(), electronicToys.toArray());
    }

    @Test
    void shouldFindNone() {
        ToyDao<ElectronicToy> electronicToyDao =new ElectronicToyDao("src/test/resources/electronic-toys-test.csv");
        SearchCriteria<ElectronicToy> criteria = new ElectronicToySearchCriteria<ElectronicToy>().add(p -> false);
        Collection<ElectronicToy> softToyCollection = electronicToyDao.find(criteria);
        Assertions.assertNotNull(softToyCollection);
        Assertions.assertTrue(softToyCollection.isEmpty());
    }

}
